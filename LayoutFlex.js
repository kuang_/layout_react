import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class LayoutFlex extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.parent1}>
            <View style={styles.parent1Sub1}>
                <View style={styles.card1}>
                  <Text>Card 1</Text>
                </View>
                <View style={styles.card2}>
                  <Text>Card 2</Text>
                </View>
            </View> 
            <View style={styles.card3}>
                <Text>Card 3</Text>
            </View>               
        </View> 
        <View style={styles.parent2}>
          <View style={styles.card4}>
                <Text>Card 4</Text>
          </View >
          <View style={styles.card5}>
                <Text>Card 5</Text>
          </View>
          <View style={styles.parent2Sub2}>
                <View style={styles.card6}>
                  <Text>Card 6</Text>
                </View>
                <View style={styles.card7}>
                  <Text>Card 7</Text>
                </View>
          </View>

        </View>               
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    // set_layout
    flex: 1,
    marginTop: 20
  },
  parent1: {
    // set_layout
    flex: 3,
    flexDirection: 'row'
  },
  parent2: {
    // set_layout
    flex: 1,
    flexDirection: 'row',
  },
  parent1Sub1: {
    // set_layout
    flex: 1,
    flexDirection: 'column'
  },
  card3: {
    // set_layout
    flex: 3,
    // set_bg-color
    backgroundColor: 'lightcoral',
    // set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  card1:{
    // set_layout
    flex: 1, 
    // set_bg-color
    backgroundColor: 'powderblue',
    // set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  card2:{
    // set_layout
    flex: 3, 
    // set_bg-color
    backgroundColor: 'skyblue',
    // set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  card4: {
    //set_layout
    flex: 1, 
    //set_bg-color
    backgroundColor: 'lightsalmon',
    // set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  card5: {
    //set_layout
    flex:2, 
    //set_bg-color
    backgroundColor: 'mediumaquamarine',
    //set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  parent2Sub2: {
    //set_layout
    flex: 1, 
    flexDirection: 'column',
    //set_bg-color
    backgroundColor: 'mediumvioletred'
  },
  card6: {
    //set_layout
    flex: 1, 
    //set_bg-color
    backgroundColor: 'powderblue',
    //set_text-align
    justifyContent: 'center',
    alignItems: 'center'
  },
  card7: {
    //set_layout
    flex: 1,
    //set_bg-color
    backgroundColor: 'skyblue',
    //set_text-align                
    justifyContent: 'center',
    alignItems: 'center'
  }
  });
