import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import LayoutFlex from './LayoutFlex'
AppRegistry.registerComponent('LayoutFlex', () => LayoutFlex);
